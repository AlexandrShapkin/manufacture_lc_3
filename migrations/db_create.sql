create table chips
(
    id            bigserial not null
        constraint chips_pk
            primary key,
    serial        varchar   not null
        constraint chips_serial_key
            unique,
    address       varchar   not null
        constraint chips_address_key
            unique,
    date          timestamp not null,
    sim_card_id   integer,
    nbiot_updated boolean default false,
    gps_updated   boolean default false
);

create unique index chips_sim_card_id_uindex
    on chips (sim_card_id);

create table programming_attempts
(
    id          bigserial not null
        constraint programming_attempts_pk
            primary key,
    date        timestamp not null,
    test_result varchar,
    status      varchar   not null,
    chip_id     bigint    not null
        constraint chips_programming_attempts_fk
            references chips
            on update cascade on delete cascade
);

create table sim_cards
(
    id               serial not null
        constraint sim_cards_pk
            primary key,
    iccid            varchar,
    imsi            varchar,
    external_id      varchar,
    configuration_id varchar,
    produced         boolean default false
);


create unique index sim_cards_configuration_id_uindex
    on sim_cards (configuration_id);

create unique index sim_cards_external_id_uindex
    on sim_cards (external_id);

create unique index sim_cards_iccid_uindex
    on sim_cards (iccid);

create unique index sim_cards_id_uindex
    on sim_cards (id);

