'''
Created on 22 дек. 2020 г.

@author: User
'''

#!/usr/bin/env python
#
# redirect data from a TCP/IP connection to a serial port and vice versa
# using RFC 2217
#
# (C) 2009-2015 Chris Liechti <cliechti@gmx.net>
#
# SPDX-License-Identifier:    BSD-3-Clause

import logging
import socket
import sys
import time
import threading
import serial.rfc2217
import pylink
import collections



class Redirector(object):
    def __init__(self, serial_instance, socket, debug=False):
        self.serial = serial_instance
        self.socket = socket
        self._write_lock = threading.Lock()
        self.rfc2217 = serial.rfc2217.PortManager(
            self.serial,
            self,
            logger=None)
            #logger=logging.getLogger('rfc2217.server') if debug else None)
        self.log = logging.getLogger('redirector')

    def statusline_poller(self):
        #self.log.debug('status line poll thread started')
        #while self.alive:
        #    time.sleep(1)
        #    self.rfc2217.check_modem_lines()
        #self.log.debug('status line poll thread terminated')
        pass

    def shortcircuit(self):
        """connect the serial port to the TCP port by copying everything
           from one side to the other"""
        '''
        self.alive = True
        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.daemon = True
        self.thread_read.name = 'serial->socket'
        self.thread_read.start()
        self.thread_poll = threading.Thread(target=self.statusline_poller)
        self.thread_poll.daemon = True
        self.thread_poll.name = 'status line poll'
        self.thread_poll.start()
        self.writer()

        self.alive = True

        self.thread_read = threading.Thread(target=self.socket_reader)
        self.thread_read.daemon = True
        self.thread_read.name = 'serial->socket'
        self.thread_read.start()
        '''

        self.processor()


    def reader(self):
        """loop forever and copy serial->socket"""
        self.log.debug('reader thread started')
        while self.alive:
            try:
                #data = self.serial.read(self.serial.in_waiting or 1)
                data = self.serial.read(4096)
                if data:
                    # escape outgoing data when needed (Telnet IAC (0xff) character)
                    self.write(b''.join(self.rfc2217.escape(data)))
            except socket.error as msg:
                self.log.error('{}'.format(msg))
                # probably got disconnected
                break
        self.alive = False
        self.log.debug('reader thread terminated')

    def write(self, data):
        """thread safe socket write with no data escaping. used to send telnet stuff"""
        with self._write_lock:
            self.socket.sendall(data)

    def processor(self):
        self.log.debug('processor thread started')
        self.socket.settimeout(0.001)
        while True:
            try:
                data = self.serial.read(4096)
                if data:
                    # escape outgoing data when needed (Telnet IAC (0xff) character)
                    self.write(b''.join(self.rfc2217.escape(data)))
                data = None
                try:
                    data = self.socket.recv(2048)
                except socket.timeout:
                    continue
                '''
                for d in serial.iterbytes(data):
                    dt = self.rfc2217.filter(d)
                    if dt:
                        self.serial.write(b''.join(dt))
                '''
                if data:
                    self.serial.write(self.rfc2217.filter(data))

                self.serial.flush_outbuf()
            except socket.error as msg:
                self.log.error('{}'.format(msg))
                # probably got disconnected
                break
        self.log.debug('processor thread terminated')

    def stop(self):
        """Stop copying"""
        self.log.debug('stopping')
        '''
        if self.alive:
            self.alive = False
            self.thread_read.join()
            self.thread_poll.join()
        '''


class RttPort(serial.SerialBase):
    SEP_BYTE = 0x00
    
    class CMDS(object):
        ZERO         = 0
        EXIT         = 1
        SET_BAUDRATE = 2
        RESET_GNSS   = 3
    
    def __init__(self, *args, **kwargs):
        self._baudrate = None
        self.name = 'RTT'
    
        self.log = logging.getLogger('rttport')
        #fh = logging.FileHandler('rttport.log')
        #formatter = logging.Formatter(fmt='%(asctime)s.%(msecs)03d %(message)s',datefmt='%Y-%m-%d,%H:%M:%S')
        #fh.setFormatter(formatter)
        
        #fh.setLevel(logging.INFO)
        #self.log.addHandler(fh)
    
        self.jlink = pylink.JLink()
    
        cw = bytes([0xa0,0xa0,0xa0,0xa0,0xa0])
        self.codeword  = collections.deque(cw, len(cw))
        self.codearray = collections.deque(len(self.codeword) * [0], len(self.codeword))

        self.need_reset = True
        self.out_buf    = []

        super().__init__(*args, **kwargs)
        
    def flush_outbuf(self):
        if self.out_buf:
            self.log.info(b'WRITE: ' + bytes(self.out_buf))
            self.jlink.rtt_write(
                0,
                bytes(self.out_buf)
            )
            self.out_buf.clear()

    @property
    def baudrate(self):
        """Get the current baud rate setting."""
        return super().baudrate

    @baudrate.setter
    def baudrate(self, baudrate):
        """\
        Change baud rate. It raises a ValueError if the port is open and the
        baud rate is not possible. If the port is closed, then the value is
        accepted and the exception is raised when the port is opened.
        """
        #self._old_baudrate = self._baudrate
        super(RttPort, self.__class__).baudrate.fset(self, baudrate)

    @property
    def bytesize(self):
        """Get the current byte size setting."""
        return super().bytesize
 
    @bytesize.setter
    def bytesize(self, bytesize):
        """Change byte size."""
        super(RttPort, self.__class__).bytesize.fset(self, bytesize)
        self.log.info('NEW BYTESIZE: ' + str(bytesize))
        
    @property
    def parity(self):
        """Get the current parity setting."""
        return super().parity

    @parity.setter
    def parity(self, parity):
        """Change parity setting."""
        super(RttPort, self.__class__).parity.fset(self, parity)
        self.log.info('NEW PARITY: ' + str(parity))

    @property
    def stopbits(self):
        """Get the current stop bits setting."""
        return super().stopbits

    @stopbits.setter
    def stopbits(self, stopbits):
        """Change stop bits size."""
        super(RttPort, self.__class__).stopbits.fset(self, stopbits)
        self.log.info('NEW STOPBITS: ' + str(stopbits))

    def open(self):
        """\
        Open port with current settings. This may throw a SerialException
        if the port cannot be opened.
        """
        self.jlink.open()
        self.jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
        self.jlink.connect("ATSAMR35J18")
        self.jlink.rtt_start()
        time.sleep(1)
        self.jlink.rtt_write(0, str.encode("AT\r\n"))
        time.sleep(1)
        #self.jlink.rtt_write(0, str.encode("AT+FWDGNSS\r\n"))
        self.jlink.rtt_write(0, str.encode("AT+FWDNBIOT\r\n"))
        time.sleep(1)
        self._old_baudrate = None
        self._reconfigure_port()
        
        self.is_open = True

    def close(self):
        """Close port"""
        pass
    
    @property
    def in_waiting(self):
        """Return the number of bytes currently in the input buffer."""
        return 1

    def read(self, size=1):
        """\
        Read size bytes from the serial port. If a timeout is set it may
        return less characters as requested. With no timeout it will block
        until the requested number of bytes is read.
        """
        result = bytes(self.jlink.rtt_read(0, size))
        if result:
            pass
            self.log.info(b'READ: ' + result)
        return result

    def write(self, data):
        """\
        Output the given byte string over the serial port. Can block if the
        connection is blocked. May raise SerialException if the connection is
        closed.
        """
        '''
        if data:
            self.codearray.extend(data)
            data = reduce(lambda S, X: S + [X] if X != self.SEP_BYTE else S + [self.SEP_BYTE, X], data, [])

            #self.jlink.rtt_write(0, data)
            self.out_buf += data

            if self.need_reset and (self.codearray == self.codeword):
                self.out_buf += [self.SEP_BYTE, self.CMDS.RESET_GNSS]
                self.log.info('Reset sent')
                self.need_reset = False
        '''
        for d in data:
            self.codearray.extend(d)
            if (d[0] == 0):
                self.out_buf += [0, 0]
            else:
                self.out_buf.append(d[0])
            if self.need_reset and (self.codearray == self.codeword):
                self.out_buf += [self.SEP_BYTE, self.CMDS.RESET_GNSS]
                self.log.info('Reset sent')
                self.need_reset = False
    
    def _update_break_state(self):
        """Set break: Controls TXD. When active, to transmitting is possible."""
        if not self.is_open:
            raise serial.PortNotOpenError()
        if self._break_state:
            pass
            #win32.SetCommBreak(self._port_handle)
        else:
            pass
            #win32.ClearCommBreak(self._port_handle)
    
    def _update_rts_state(self):
        """Set terminal status line: Request To Send"""
        if self._rts_state:
            #win32.EscapeCommFunction(self._port_handle, win32.SETRTS)
            pass
        else:
            #win32.EscapeCommFunction(self._port_handle, win32.CLRRTS)
            pass
    
    def _update_dtr_state(self):
        """Set terminal status line: Data Terminal Ready"""
        if self._dtr_state:
            #win32.EscapeCommFunction(self._port_handle, win32.SETDTR)
            pass
        else:
            #win32.EscapeCommFunction(self._port_handle, win32.CLRDTR)
            pass
    
    def _reconfigure_port(self):
        """Set communication parameters on opened port."""
        if self._old_baudrate != self._baudrate:
            self._old_baudrate = self._baudrate
            self.log.info('NEW BAUD: ' + str(self._baudrate))
            
            self.flush_outbuf()
            self.out_buf += (
                [self.SEP_BYTE, self.CMDS.SET_BAUDRATE] +
                list(self._baudrate.to_bytes(4, byteorder='little', signed=False))
            )
            self.flush_outbuf()
    
    @property
    def cts(self):
        """Read terminal status line: Clear To Send."""
        return True

    @property
    def dsr(self):
        """Read terminal status line: Data Set Ready."""
        return True

    @property
    def ri(self):
        """Read terminal status line: Ring Indicator."""
        return True

    @property
    def cd(self):
        """Read terminal status line: Carrier Detect."""
        return True

def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="RFC 2217 Serial to Network (TCP/IP) redirector.",
        epilog="""\
NOTE: no security measures are implemented. Anyone can remotely connect
to this service over the network.
Only one connection at once is supported. When the connection is terminated
it waits for the next connect.
""")

    parser.add_argument(
        '-p', '--localport',
        type=int,
        help='local TCP port, default: %(default)s',
        metavar='TCPPORT',
        default=2217)

    parser.add_argument(
        '-v', '--verbose',
        dest='verbosity',
        action='count',
        help='print more diagnostic messages (option can be given multiple times)',
        default=0)

    args = parser.parse_args()

    if args.verbosity > 3:
        args.verbosity = 3
    level = (logging.WARNING,
             logging.INFO,
             logging.DEBUG,
             logging.NOTSET)[args.verbosity]
    logging.basicConfig(level=logging.INFO)
    #~ logging.getLogger('root').setLevel(logging.INFO)
    logging.getLogger('rfc2217').setLevel(level)
    
    ##############################################
    
    # connect to serial port
    ser = RttPort()

    logging.info("RFC 2217 TCP/IP to Serial redirector - type Ctrl-C / BREAK to quit")

    try:
        ser.open()
    except serial.SerialException as e:
        logging.error("Could not open serial port {}: {}".format(ser.name, e))
        sys.exit(1)

    logging.info("Serving serial port: {}".format(ser.name))
    #settings = ser.get_settings()

    srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    srv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    srv.bind(('', args.localport))
    srv.listen(1)
    logging.info("TCP/IP port: {}".format(args.localport))
    while True:
        try:
            client_socket, addr = srv.accept()
            logging.info('Connected by {}:{}'.format(addr[0], addr[1]))
            client_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            ser.rts = True
            ser.dtr = True
            # enter network <-> serial loop
            r = Redirector(
                ser,
                client_socket,
                args.verbosity > 0)
            try:
                r.shortcircuit()
            finally:
                logging.info('Disconnected')
                r.stop()
                client_socket.close()
                ser.dtr = False
                ser.rts = False
                # Restore port settings (may have been changed by RFC 2217
                # capable client)
                #ser.apply_settings(settings)
        except KeyboardInterrupt:
            sys.stdout.write('Keyboard Interrupt\n')
            break
        except socket.error as msg:
            logging.error(str(msg))

    logging.info('--- exit ---')

if __name__ == '__main__':
    main()