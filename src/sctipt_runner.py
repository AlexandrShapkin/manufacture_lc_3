from datetime import datetime

import psutil
import subprocess
import time
from queue import Queue, Empty
from threading import Thread
import logging

from programmer import Programmer

CHIP_NAME = 'ATSAMG55J19'

class ScriptRunner:
    scr1_NB = 'python ext/BC66_fw_updater/rfc2217_rtt_forwarder_NB.py -p 4444'
    scr1_GPS = 'python ext/N30B_upgrade/rfc2217_rtt_forwarder_GPS.py -p 4444'
    scr2 = 'ext\hub2com\com2tcp-rfc2217.bat \\\\.\\{com} localhost 4444'

    # scr3 = '/ext/N30B_upgrade/Utility/FlashTool_Sample.exe 3 460800 1 ext/N30B_upgrade/Files/N30BGv01.G.R1/N30BGv01.G.R1_AllInOne_DA_MT3333.bin ext/N30B_upgrade/Files/N30BGv01.G.R1/N30BGv01.G.R1.bin ext/N30B_upgrade/Filesscat.txt 1'
    # scr3 = '/ext/N30B_upgrade/Utility/FlashTool_Sample.exe {com_num} 460800 1 ext/N30B_upgrade/Files/N30BGv01.G.R1/N30BGv01.G.R1_AllInOne_DA_MT3333.bin ext/N30B_upgrade/Files/N30BGv01.G.R1/N30BGv01.G.R1.bin ext/N30B_upgrade/Filesscat.txt 1'

    nb_target_version = 'BC66NBR01A11_01.003.01.003'

    key_scr1_ready = 'INFO:root:TCP/IP port: 4444'
    key_scr2_ready = 'TCP(1): Connected'
    UPDATE_NB_TIMEOUT = 60 * 5
    UPDATE_GPS_TIMEOUT = 60 * 5

    def __init__(self, sp, config_yaml, application_path):
        self.sp = sp
        self.config_yaml = config_yaml
        self.application_path = application_path
        self.proc1 = None
        self.proc2 = None
        self.proc3 = None
        self.proc1_q = None
        self.proc2_q = None

    @staticmethod
    def enqueue_output(out, queue):
        try:
            for line in iter(out.readline, b''):
                res = line.rstrip()
                if res != '':
                    queue.put(res)
                    print(res)
                    logging.debug(res)
            out.close()
        except:
            pass

    def update_nbiot(self):
        self._run_script1_async()
        self._run_script2_async()
        self._run_script_nb_update()

    def update_gps(self):
        self._run_script1_async(gps=True)
        self._run_script2_async()
        time.sleep(2)
        self._run_script_gps_update()

    def _run_script1_async(self, gps=False):
        self.sp.start('Запуск 1 скрипта обновления')

        scr = self.scr1_GPS if gps else self.scr1_NB

        self.proc1 = subprocess.Popen(scr, stderr=subprocess.PIPE, stdout=subprocess.PIPE, text=True)
        # sp.(f'proc1 pid - {proc1.pid}')
        self.proc1_q = Queue()
        proc1_reader = Thread(target=ScriptRunner.enqueue_output, args=(self.proc1.stderr, self.proc1_q), daemon=True,
                              name='proc1_reader')
        proc1_reader.start()
        script1_ready = False

        while self.proc1.poll() is None:
            try:
                out = self.proc1_q.get_nowait()
            except Empty:
                pass
            else:
                if 'Exception' in out:
                    if 'JLinkException' in out:
                        e_mess = 'ERROR: Проверьте JLink, питание модуля...'
                        self.sp.warn(e_mess)
                    else:
                        e_mess = out
                        self.sp.warn(f'ERROR: {e_mess}')
                    raise Exception(e_mess)
                elif self.key_scr1_ready in out:
                    script1_ready = True
                    break

        if script1_ready:
            time.sleep(2)
            self.sp.succeed("Cкрипт 1 запущен")
        else:
            self.sp.fail("1 скрипт обновления не запустился")
            raise Exception('1 скрипт обновления не запустился')

    def _run_script2_async(self):
        self.sp.start('Запуск 2 скрипта обновления')
        self.proc2 = subprocess.Popen(f"{self.scr2.format(com=self.config_yaml.get('com_port_loop').get('com_out'))}",
                                      stderr=subprocess.PIPE, stdout=subprocess.PIPE, text=True)
        logging.debug(f'proc2 pid - {self.proc2.pid}')

        self.proc2_q = Queue()
        proc2_reader = Thread(target=ScriptRunner.enqueue_output, args=(self.proc2.stdout, self.proc2_q), daemon=True,
                              name='proc2_reader')
        proc2_reader.start()
        script2_ready = False

        while self.proc2.poll() is None:
            try:
                out = self.proc2_q.get_nowait()
            except Empty:
                pass
            else:
                if 'Exception' in out:
                    self.sp.warn(f'unexpected error: {out}')
                    # todo raise error
                    # break
                elif self.key_scr2_ready in out:
                    script2_ready = True
                    break
        #
        if script2_ready:
            time.sleep(1)
            self.sp.succeed("Cкрипт 2 запущен")
        else:
            self.sp.fail("2 скрипт обновления не запустился")
            o, e = self.proc2.communicate()
            if e != '':
                raise Exception(e)
            raise Exception('2 скрипт обновления не запустился')

    def _run_script_nb_update(self):
        script3_ready = False
        self.sp.info('Займет примерно 2,5 минуты')
        self.sp.start('Обновление NBIoT модуля')

        self.proc3 = psutil.Popen(f"QFlash_V4.13_M66_BC66.exe {self.config_yaml.get('com_port_loop').get('com_in')[3:]} 921600 {self.application_path}\\ext\\BC66_fw_updater\\BC66NBR01A11_01.003.01.003\\flash_download.cfg",
                                  cwd=f'{self.application_path}\\ext\\BC66_fw_updater\\QFlash_V4.13_M66_BC66_20190918\\Release',
                                  shell=True)

        key = 'iZ'
        #    5 12 SE
        triger_1 = False
        e_key = 'INFO:rttport:b\'WRITE: \\xa0\''
        e_count = 0
        st = datetime.now()
        last_out = datetime.now()
        while self.proc1.poll() is None and self.proc2.poll() is None:
            if (datetime.now() - st).seconds > self.UPDATE_NB_TIMEOUT:
                e_mess = f'Процесс обновления занимает больше {self.UPDATE_NB_TIMEOUT} секунд, скорее всего что-то пошло не так...'
                self.sp.fail(e_mess)
                raise Exception(e_mess)
            try:
                out = self.proc1_q.get_nowait()
                last_out = datetime.now()
            except Empty:
                if (datetime.now() - last_out).seconds > 30:
                    # fixme
                    pass
                    # raise Exception("Процесс обновления прерван, запустите еще раз")
                # pass
            else:
                if 'Exception' in out:
                    pass
                elif e_key == out:
                    e_count += 1
                    if e_count > 50:
                        self.sp.fail('Не удалось плдключиться к NBIoT контроллеру')
                        raise Exception('Не удалось плдключиться к NBIoT контроллеру')
                elif "INFO:rttport:b\'READ:" in out and key in out:
                    if triger_1:
                        script3_ready = True
                        time.sleep(10)
                        break
                    triger_1 = True
                else:
                    e_count = 0

                if script3_ready:
                    self.sp.succeed('Прошивка NBIoT записана')

        if not script3_ready:
            raise Exception('Обновление NBIoT не удалось')

    def _run_script_gps_update(self):
        # time.sleep(10)
        st = datetime.now()
        script3_ready = False
        self.sp.info('Займет примерно 2.5 минуты')
        self.sp.start('Обновление GPS модуля')

        # 460800
        # 230400
        # 115200
        # self.proc4 = psutil.Popen(f"FlashTool_Sample.exe {'COM20'[3:]} 115200 1 ..\\Files\\N30BGv01.G.R1\\N30BGv01.G.R1_AllInOne_DA_MT3333.bin ..\\Files\\N30BGv01.G.R1\\N30BGv01.G.R1.bin ..\\Files\\scat.txt 1",
        self.proc4 = psutil.Popen(f"FlashTool_Sample.exe {self.config_yaml.get('com_port_loop').get('com_in')[3:]} 460800 1 ..\\Files\\N30BGv01.G.R1\\N30BGv01.G.R1_AllInOne_DA_MT3333.bin ..\\Files\\N30BGv01.G.R1\\N30BGv01.G.R1.bin ..\\Files\\scat.txt 1",

                                  cwd=f'{self.application_path}\\ext\\N30B_upgrade\\Utility',
                                  shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        try:
            o, e = self.proc4.communicate(timeout=self.UPDATE_GPS_TIMEOUT)
        except Exception as er:
            raise Exception(f'Процесс обновления GPS занимает больше {self.UPDATE_GPS_TIMEOUT} секунд, скорее всего что-то пошло не так...')

        if 'FW download succeed!' not in o.decode():

            if 'PMTKVNED' in o.decode():
                programmer = Programmer()
                programmer.connect(CHIP_NAME)
                programmer.reset()
                programmer.restart()
                self.sp.start('Перезагрузка модуля')
                time.sleep(10)
                self.sp.succeed('Модуль готов к обнолвению переферийных контроллеров')
                programmer.jlink.close()

            raise Exception(f"Не удалось обновть GPS - {o.decode()[-80:]}")


        self.sp.succeed('Прошивка GPS записана')
        print((datetime.now() - st).seconds)

    def clean_procs(self):
        if self.proc1 is not None:
            if self.proc1.poll() is None: self.proc1.kill()
        if self.proc2 is not None:
            if self.proc2.poll() is None:
                for child in psutil.Process(self.proc2.pid).children(recursive=True):
                    if child.is_running(): child.kill()
                self.proc2.kill()
        if self.proc3 is not None:
            if self.proc3.poll() is None:
                for child in psutil.Process(self.proc3.pid).children(recursive=True):
                    if child.is_running(): child.kill()
                self.proc2.kill()

        if self.proc4 is not None:
            if self.proc4.poll() is None:
                for child in psutil.Process(self.proc4.pid).children(recursive=True):
                    if child.is_running(): child.kill()
                self.proc2.kill()
