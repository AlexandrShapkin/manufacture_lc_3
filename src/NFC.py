from smartcard.System import readers
from datetime import datetime
from smartcard.CardRequest import CardRequest




class NFC:

    cmdMap = {
        "mute":[0xFF, 0x00, 0x52, 0x00, 0x00],
        "unmute":[0xFF, 0x00, 0x52, 0xFF, 0x00],
        "getuid":[0xFF, 0xCA, 0x00, 0x00, 0x00],
        "firmver":[0xFF, 0x00, 0x48, 0x00, 0x00],
    }

    def __init__(self):
        r = readers()
        if len(r) < 1:
            raise Exception("NFC Reader не подключен")

        # self.connection = r[0].createConnection()
        # self.connection.connect()


    def writePage(self, connection, page, value):
        # self.connection.transmit()
        # handshake
        # COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00]
        # self.connection.transmit(COMMAND)


        WRITE_COMMAND = [0xFF, 0xD6, 0x00, int(page), 0x04, int(value[0:2], 16), int(value[2:4], 16), int(value[4:6], 16), int(value[6:8], 16)]
        resp = connection.transmit(WRITE_COMMAND)
        if resp[1] != 144:
            raise Exception("Не удалось записать NFC")
            # print("Wrote " + value + " to page " + str(page))

    def readPage(self, page):

        cardrequest = CardRequest()
        cardservice = cardrequest.waitforcard()
        with cardservice.connection as connection:
            connection.connect()


            resp = connection.transmit([0xFF, 0xB0, 0x00, int(page), 0x04])
            dataCurr = NFC.stringParser(resp)

            if(dataCurr is not None):
                return dataCurr
            else:
                raise Exception("Не cчитать записать NFC")




    @staticmethod
    def stringParser(dataCurr):
        #--------------String Parser--------------#
        #([8    5, 203, 230, 191], 144, 0) -> [85, 203, 230, 191]
        if isinstance(dataCurr, tuple):
            temp = dataCurr[0]
            code = dataCurr[1]
        #[85, 203, 230, 191] -> [85, 203, 230, 191]
        else:
            temp = dataCurr
            code = 0

        dataCurr = ''

        #[85, 203, 230, 191] -> bfe6cb55 (int to hex reversed)
        for val in temp:
            # dataCurr += (hex(int(val))).lstrip('0x') # += bf
            dataCurr += format(val, '#04x')[2:] # += bf

        #bfe6cb55 -> BFE6CB55
        dataCurr = dataCurr.upper()

        #if return is successful
        if (code == 144):
            return dataCurr

    def writeTag(self, device_class, device_id, dt=datetime.now()):

        cardrequest = CardRequest(timeout=600)
        cardservice = cardrequest.waitforcard()
        with cardservice.connection as connection:
            connection.connect()

            self.writePage(connection, 4, '{:08x}'.format(device_class))
            self.writePage(connection, 5, device_id[:8])
            self.writePage(connection, 6, device_id[8:])
            tmstmp = "{:016x}".format(int(dt.timestamp()))
            self.writePage(connection, 7,tmstmp[:8])
            self.writePage(connection, 8,tmstmp[8:])


if __name__ == '__main__':
    n = NFC()
    while True:
        try:
            ser = input("Отсканируйте этикетку: ")

            if not ser.startswith("ACN"):
                raise Exception("Неверный формат")

            id = ser.split("-")[-1]
            cl = int(ser.split("-")[0].split(":")[1])

            input("Поднесите 1 метку и нажмите Enter")
            n.writeTag(cl, id)
            input("Поднесите 2 метку и нажмите Enter")
            n.writeTag(cl, id)
            print("ok")
        except Exception as e:
            print(e)


