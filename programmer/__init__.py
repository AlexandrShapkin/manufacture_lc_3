import pylink
import sys
import logging

from .read_chip_serails import read_serial_from_ATSAMG55J19, read_serial_from_ATSAMR35J18
import time
from func_timeout import func_timeout, FunctionTimedOut


try:
    import thread
except ImportError:
    import _thread as thread

# setting stop bit for 102 controller
ADDR_EEFC_FCR = int("0x400E0A04", 16)




class Programmer(object):

    def __init__(self):
        self.jlink = None
        self.timeout = None

        try:
            # print("Initializing Jlink drivers...")
            self.jlink = pylink.JLink()
        except Exception as e:
            print("ERROR - " + str(e))
            input("Check your JLink drivers, press Enter to exit...")
            sys.exit()

        try:
            # print("Initializing JLink device...")
            self.jlink.open()
            self.jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
            # print("JLink device initialized")
        except Exception as e:
            print("ERROR: " + str(e))
            # input("Maybe you have to check your JLink device, press Enter to exit...")
            sys.exit()

    def flash_byte_array(self, byte_array, shift=0):
        if self.connected():
            # print("Flashing byte array")
            self.jlink.flash_write8(shift, byte_array)
            # print("Flashed array")

    def flash(self, file_path, shift=0):
        if self.connected():
            # print("Flashing file - " + file_path)
            self.jlink.flash_file(file_path, shift)
            # print("File flashed")

    def erase(self):
        if self.connected():
            # print("Erasing chip...")
            self.jlink.erase()
            # print("done")

    def reset(self):
        if self.connected():
            self.jlink.reset()

    def restart(self):
        if self.connected():
            self.jlink.restart()

    def connect(self, chip_name, speed=3000):
        try:
            logging.debug("Connecting to chip - " + chip_name +", speed - " + str(speed))
            self.jlink.connect(chip_name, speed=speed)
            logging.debug('conencted to chip')
        except Exception as e:
            print("ERROR: " + str(e))
            raise Exception('Check if your device plugged-in...')

    def connected(self):
        if self.jlink.connected():
            return True
        else:
            raise Exception('Device disconncted!')

    def start_rtt(self):
        if self.connected():
            # print("Starting RTT...")
            self.jlink.rtt_start()
            # print("Started RTT...")

    def read_timeout(self, timeout):
        time.sleep(timeout)
        if self.timeout is not None:
            self.timeout = True

    class TimeoutException(Exception):
        pass

    def _read_rtt_internal(self):
        while self.jlink.connected():
            terminal_bytes = self.jlink.rtt_read(0, 1024)
            if terminal_bytes:
                s = "".join(map(chr, terminal_bytes))
                sys.stdout.flush()
                return s
            time.sleep(0.1)

    def read_rtt(self, timeout=10, raiseTimeoutEx=True):

        try:
            return func_timeout(timeout, self._read_rtt_internal)
        except FunctionTimedOut:
            if raiseTimeoutEx:
                raise self.TimeoutException()
        except Exception:
            print("IO read thread exception, exiting...")
            thread.interrupt_main()
            raise

    def get_chip_serial_ATSAMG55J19(self):
        if self.connected():
            self.jlink.halt()
            self.jlink.reset()
            self.jlink.restart()
            self.set_boot_bit()
            r = read_serial_from_ATSAMG55J19(self.jlink)
            self.jlink.restart()
            return r

    def get_chip_serial_ATSAMR35J18(self):
        if self.connected():
            self.jlink.halt()
            self.jlink.reset()
            r = read_serial_from_ATSAMR35J18(self.jlink)
            self.jlink.restart()
            return r

    def writeToRTT(self, str):
        bytes = list(bytearray(str, "utf-8") + b"\x0D\x0A")
        try:
            bytes_written = self.jlink.rtt_write(0, bytes)
        except:
            raise Exception("Error while sendig command via JLink")

        if len(bytes) != bytes_written:
            raise Exception("Error while sendig command via JLink(send & message length different)")

    def writeToRTTAssync(self, str):
        thread.start_new_thread(self.writeToRTT, (str, ))

    def set_boot_bit(self):
        '''Function for writing 1 to GPNVM bit 1 to run firmwares from flash at startup'''
        self.jlink.reset()
        dat_write = int(1509949707)
        self.jlink.memory_write32(ADDR_EEFC_FCR, [dat_write])
        # print("stop bit was set")

    def set_security_bit(self):
        '''Function for writing 1 to GPNVM bit 0 to forbid fw write and read operations via SWD'''
        # jlink.reset() #NOTE MCU bricks if setting security bit just after reset
        # dat_write = int(np.uint32(np.uint8(CMD_SGPB) | np.uint8(0) << 8 | np.uint8(0) << 16 | np.uint8(FKEY) << 24))
        dat_write = int(1509949451)
        self.jlink.memory_write32(ADDR_EEFC_FCR, [dat_write])
        time.sleep(0.5)
        self.jlink.close()
