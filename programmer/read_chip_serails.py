import numpy as np
import time


# ADDR_EEFC_FSR = int("0x400E0A08", 16)
# ADDR_EEFC_FRR = int("0x400E0A0C", 16)
# ADDR_EEFC_FCR = int("0x400E0A04", 16)
# CMD_SGPB = int("0x0B", 16)
# CMD_STUI = int("0x0E", 16)
# CMD_SPUI = int("0x0F", 16)
# FKEY = int("0x5A", 16)

def read_serial_from_ATSAMG55J19(jlink):
    ADDR_EEFC_FSR = int("0x400E0A08", 16)
    ADDR_EEFC_FRR = int("0x400E0A0C", 16)
    ADDR_EEFC_FCR = int("0x400E0A04", 16)
    CMD_SGPB = int("0x0B", 16)
    CMD_STUI = int("0x0E", 16)
    CMD_SPUI = int("0x0F", 16)
    FKEY = int("0x5A", 16)

    dat_write = int(np.uint32(np.uint8(CMD_STUI) | np.uint8(0) << 8 | np.uint8(0) << 16 | np.uint8(FKEY) << 24))
    bytes_write = dat_write.to_bytes(4, byteorder='little', signed=False)
    jlink.memory_write32(ADDR_EEFC_FCR, [dat_write])
    time.sleep(.1)
    uniqueID = jlink.memory_read8(0, 16)

    dat_write = int(np.uint32(np.uint8(CMD_SPUI) | np.uint8(0) << 8 | np.uint8(0) << 16 | np.uint8(FKEY) << 24))
    bytes_write = dat_write.to_bytes(4, byteorder='little', signed=False)
    jlink.memory_write32(ADDR_EEFC_FCR, [dat_write])
    time.sleep(.1)
    status = jlink.memory_read8(ADDR_EEFC_FSR, 4)

    return bytes_to_string(uniqueID)


def read_serial_from_ATSAMR35J18(jlink):
    W0 = int('0x0080A00C', 16)
    W1 = int('0x0080A040', 16)
    W2 = int('0x0080A044', 16)
    W3 = int('0x0080A048', 16)

    w_arr = [W0, W1, W2, W3]

    res = []
    for w in w_arr:
        print(str(w))
        res.extend(jlink.memory_read8(w, 4))

    return bytes_to_string(res)


def bytes_to_string(byte_arr):
    text_str = ''
    for byte in bytes(byte_arr):
        temp = hex(byte)
        if len(temp) == 3:
            temp = '0x0' + temp[-1:]
        text_str = text_str + temp[2:]

    return text_str
