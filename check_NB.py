import time

from manufacture_database import Databse
from programmer import Programmer

CHIP_NAME = 'ATSAMG55J19'
KEY_DEVICE_START = 'DEVICE_READY'
COMMAND_GET_SIM_ICCID = 'AT+NBIOTCMD=AT+QCCID'

if __name__ == '__main__':
    db = Databse()

    f = open("info.txt", "a")
    while True:
        input("Подключите устройство и нажмите Enter...")
        programmer = Programmer()
        programmer.connect(CHIP_NAME)
        time.sleep(3)
        programmer.set_boot_bit()
        chip_serial = programmer.get_chip_serial_ATSAMG55J19()

        chip_record = db.get_chip_record(chip_serial)
        if chip_record is None:
            print("ERR gettting record inf")
            continue

        device_id = chip_record[2]

        # programmer.restart()
        programmer.jlink.rtt_start()
        time.sleep(3)
        out = programmer.read_rtt(180)
        # print("RTT - " + str(out))
        if KEY_DEVICE_START in out:
            print('Модуль запущен')
        else:
            print("ERR stsrting device")
            continue

        print('Получение ICCID сим-карты')
        programmer.writeToRTT(COMMAND_GET_SIM_ICCID)
        time.sleep(3)
        out = programmer.read_rtt()
        # print(out)
        if "OK" in out:
            pass
        else:
            print("ERR getting icid 1")
            continue
        if 'QCCID' not in out:
            print("ERR getting icid")
            continue

        iccid = out[out.find('+QCCID:') + 8:].split('\r\n')[0]

        f.write(f"{device_id, iccid}")






