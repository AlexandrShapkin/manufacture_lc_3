import psycopg2
from deprecated import deprecated


class Databse:

    REEQUEST_GET_CHIP_BY_CHIP_ID = '''
    SELECT * FROM chips LEFT JOIN sim_cards on chips.sim_card_id = sim_cards.id WHERE serial =  %(serial)s
    '''

    REQUEST_ADD_CHIP = '''
    INSERT INTO chips(serial, address, date) VALUES (%(serial)s, %(address)s, current_timestamp) RETURNING id
    '''

    REQUEST_ADD_ATTEMPT = '''
    INSERT INTO programming_attempts(chip_id, status, date) VALUES (%(chip_id)s, 'STARTED', current_timestamp) RETURNING id
    '''

    REQUEST_UPDATE_ATTEMPT_STATUS = '''
    UPDATE programming_attempts SET status = %(status)s, date = current_timestamp  WHERE id = %(attempt_id)s
    '''

    REQUETS_ADD_TEST_RESULTS = '''
    UPDATE programming_attempts SET test_result = %(test_result)s, date = current_timestamp WHERE id = %(attempt_id)s
    '''

    REQUEST_GET_SIM_BY_ICCID = '''
    select * from sim_cards where iccid = %(iccid)s
    '''

    REQUEST_CHECK_IF_HAS_SUCCESS = '''
    SELECT serial, status from chips JOIN programming_attempts pa on chips.id = pa.chip_id where status = 'SUCCESS' and serial = %(serial)s
    '''

    def __init__(self):
        # self.connection = psycopg2.connect(user="postgres",
        #                                    password="UPHfzpwoctCfSjco",
        #                                    host="localhost",
        #                                    port="5432",
        #                                    database="manufacture_LCL_2")

        self.connection = psycopg2.connect(
                                            user="lcn2_user",
                                            password="MMKxv5ZZfT96MZHh",
                                           host="rc1c-76od9db6ln166lcm.mdb.yandexcloud.net",
                                           port=6432,
                                           database="manufacture_lcn_2",
                                           sslmode="verify-ca",
                                           sslrootcert="CA.pem"
                                           )

        self.cursor = self.connection.cursor()

    def get_chip_record(self, chip_id):
        self.cursor.execute(self.REEQUEST_GET_CHIP_BY_CHIP_ID, {"serial": chip_id})
        self.connection.commit()
        return self.cursor.fetchone()

    def add_chip(self, chip_id, device_id):
        self.cursor.execute(self.REQUEST_ADD_CHIP, {'serial': chip_id, 'address': device_id})
        self.connection.commit()
        return self.cursor.fetchone()

    def start_attempt(self, chip_id):
        self.cursor.execute(self.REQUEST_ADD_ATTEMPT, {'chip_id': chip_id})
        self.connection.commit()
        return self.cursor.fetchone()

    def update_attempt_status(self, attempt_id, status):
        self.cursor.execute(self.REQUEST_UPDATE_ATTEMPT_STATUS, {'attempt_id': attempt_id, 'status': status})
        self.connection.commit()
        # return self.cursor.fetchone()

    def add_test_results(self, attempt_id, results):
        self.cursor.execute(self.REQUETS_ADD_TEST_RESULTS, {'attempt_id': attempt_id, 'test_result': results})
        self.connection.commit()
        # return self.cursor.fetchone()

    @deprecated
    def get_sim_by_iccid(self, iccid):
        self.cursor.execute(self.REQUEST_GET_SIM_BY_ICCID, {"iccid": iccid})
        self.connection.commit()
        return self.cursor.fetchone()


    def check_if_serial_hass_success_attempt(self, chip_id):
        self.cursor.execute(self.REQUEST_CHECK_IF_HAS_SUCCESS, {"serial": chip_id})
        self.connection.commit()
        res = self.cursor.fetchone()
        return res is not None and len(res) > 0

    @deprecated
    def update_sim_config_id(self, sim_id, conf_id):
        self.cursor.execute('UPDATE sim_cards SET configuration_id = %(conf_id)s  WHERE id = %(sim_id)s',
                            {'conf_id': conf_id, 'sim_id': sim_id})
        self.connection.commit()

    @deprecated
    def update_sim_id_for_device(self, device_id, sim_id):
        self.cursor.execute('UPDATE chips SET sim_card_id = %(sim_id)s  WHERE id = %(device_id)s',
                            {'device_id': device_id, 'sim_id': sim_id})
        self.connection.commit()

    def set_nbiot_updated(self, chip_id, updated=True):
        self.cursor.execute('UPDATE chips SET nbiot_updated = %(updated)s  WHERE id = %(chip_id)s',
                            {'chip_id': chip_id, "updated": updated})
        self.connection.commit()

    def set_gps_updated(self, chip_id, updated=True):
        self.cursor.execute('UPDATE chips SET gps_updated = %(updated)s  WHERE id = %(chip_id)s',
                            {'chip_id': chip_id, "updated": updated})
        self.connection.commit()

    @deprecated
    def set_sim_produced(self, sim_id):
        self.cursor.execute('UPDATE sim_cards SET produces = true  WHERE id = %(sim_id)s',
                            {'sim_id': sim_id})
        self.connection.commit()






