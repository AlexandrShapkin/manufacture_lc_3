import sys
import usb.core
import usb.util
import usb.backend.libusb1
from datetime import datetime
import array

LABEL_TEMPLATE = '''
   CT~~CD,~CC^~CT~
 CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW1394
^LL0283
^LS035
^FT340,185^BQN,2,7
^FH\^FDLA,ACN:%%DEVICE_CLASS%%-%%SERIAL%%^FS
^FT79,168^A0N,36,36^FH\^FDACN:%%DEVICE_CLASS%%-^FS
^FT79,212^A0N,36,36^FH\^FD%%SERIAL%%^FS
^FT76,119^A0N,27,26^FH\^FD%%DATE%%^FS
^FT860,185^BQN,2,7
^FH\^FDLA,ACN:%%DEVICE_CLASS%%-%%SERIAL%%^FS
^FT598,168^A0N,36,36^FH\^FDACN:%%DEVICE_CLASS%%-^FS
^FT598,212^A0N,36,36^FH\^FD%%SERIAL%%^FS
^FT595,119^A0N,27,26^FH\^FD%%DATE%%^FS
^PQ1,0,1,Y^XZ
    '''


class LabelPrinter:

    def __init__(self, idVendor=0x0a5f, idProduct=0x00AB):
        print("Initing label printer")

        try:
            backend = usb.backend.libusb1.get_backend(find_library=lambda
                x: "C:\Windows\System32\DriverStore\FileRepository\libusb-win32_generic_device.inf_amd64_neutral_d9bb4a9d4bb0a4d2\x86\libusb0_x86.dll")
            self.printer = usb.core.find(idVendor=idVendor, idProduct=idProduct, backend=backend)
        except Exception as e:
            print(e)
            input("Can't get printer via USB, press Enter to exit...")
            sys.exit()

        if self.printer is None:
            input("Printer was not fount, press Enter to exit...")
            sys.exit()

        print("Label printer inited")

    def print(self, serial, device_class):
        print("Printing label for - " + serial)
        s = LABEL_TEMPLATE.replace("%%SERIAL%%", serial).replace("%%DATE%%", datetime.now().strftime("%d.%m.%Y")).replace('%%DEVICE_CLASS%%', device_class)
        a = array.array('B')
        a.frombytes(bytearray(s, 'utf-8'))
        self.printer.write(1, a)
        print("Label printed")
