import time
import stomp
import uuid
import logging
import json

from enum import Enum, auto
from datetime import datetime

from api.back import check_dev_eui, create_device


class AMLoraServiceCore:
    SEND_QUEUE = "manufacture"
    LISTEN_QUEUE = "main.manufacture"

    USERNAME = 'am_prod_manufacture_script'
    PASSWORD = '6X2GbVuZXXRw8VvA'

    MESSAGE_TIMEOUT = 30

    class JMSType(Enum):
        createConfiguration = auto()
        deleteConfiguration = auto()

    # def __init__(self, host='172.21.22.5', port=61613, username=USERNAME, password=PASSWORD):
    def __init__(self, host='172.21.21.10', port=61613, username=USERNAME, password=PASSWORD):
        self._connected = None
        self._message = None
        self._got_uplink = False
        self._uplink_ext_id = None
        self._enq_id = None

        self._conn = stomp.Connection([(host, port)], auto_content_length=False)

        self._listner = stomp.ConnectionListener()
        self._listner.on_connected = self._on_connected
        self._listner.on_disconnected = self._on_disconnected
        self._listner.on_message = self._on_message

        self._conn.set_listener(AMLoraServiceCore.LISTEN_QUEUE, self._listner)

        self._conn.connect(username, password, wait=True)
        self._conn.subscribe(destination=AMLoraServiceCore.LISTEN_QUEUE, id=AMLoraServiceCore.LISTEN_QUEUE, ack='auto')

    def _on_connected(self, frame):
        logging.info("MQ connected")
        self._connected = True

    def _on_disconnected(self):
        logging.info("MQ disconnected")
        self._connected = False

    def _on_message(self, frame):
        jmess = json.loads(frame.body)

        if frame.headers.get("type") == "uplink":
            if self._uplink_ext_id == jmess.get("externalId"):
                self._got_uplink = True
            return

        if jmess.get("comId") != self._enq_id:
            return

        self._message = jmess

    def set_wait_uplink_ext_id(self, ext_id):
        self._uplink_ext_id = ext_id

    def set_uplink_waiter(self, ext_id=None, timeout=30):
        if ext_id != None:
            self._uplink_ext_id = ext_id

        st = datetime.now()
        while (datetime.now() - st).seconds < timeout and not self._got_uplink:
            time.sleep(1)

        if not self._got_uplink:
            raise Exception("Аплинк-сообщение не было получено")

        self._got_uplink = False
        self._uplink_ext_id = None
        return True

    def send(self, message, jms_type, message_id):

        if self._enq_id is not None or not self._connected:
            raise Exception(f"MqMessager не готов {self._enq_id is None, self._connected}")

        self._enq_id = message_id
        self._conn.send(body=message, destination=AMLoraServiceCore.SEND_QUEUE, headers={'type': jms_type.name})

        st = datetime.now()
        while (datetime.now() - st).seconds < self.MESSAGE_TIMEOUT and self._message is None:
            time.sleep(1)

        self._enq_id = None
        if self._message is None:
            raise Exception("Не получен ответ от сервера")


        return self._message

    def create_sim_conf(self, external_id, manufacture=True):

        com_id = str(uuid.uuid4())

        message = json.dumps({
            "comId": com_id,
            "networkServer": "MTS",
            "data": {
                "externalId": external_id,
                "manufacture": manufacture
            }
        })

        res = self.send(message, AMLoraServiceCore.JMSType.createConfiguration, com_id)

        if not res.get("success"):
            raise Exception(f"Не удлось создать конфигурацию для extId: {external_id}, {res.get('error')}")

        return res.get("data").get("configurationId")

    def delete_sim_conf(self, conf_id):
        com_id = str(uuid.uuid4())

        message = json.dumps({
            "comId": com_id,
            "networkServer": "MTS",
            "data": {
                "configurationId": conf_id
            }
        })

        res = self.send(message, AMLoraServiceCore.JMSType.deleteConfiguration, com_id)

        if not res.get("success"):
            raise Exception(f"Не удлось удалить конфигурацию - {conf_id}")
        return True

    def disconnct(self):
        if self._connected:
            self._conn.disconnect()


if __name__ == '__main__':

    res = check_dev_eui("1000000000020001")
    res  = create_device('1000000000020001', "250015140000901@89701010051400009013.ambiot.nidd", "605c58380a0b009200014ed2",
                         '89701010051400009013', '250015140000901')


    # -----
    ext_id = "250015140000901@89701010051400009013.ambiot.nidd"

    mq = AMLoraServiceCore()
    while not mq._connected:
        time.sleep(1)

    # res = mq.create_sim_conf(ext_id, manufacture=False)
    # res = mq.delete_sim_conf('605c58380a0b009200014ed2')
    print(res)

    x = 1

    # res = mq.set_uplink_waiter(ext_id, 120)
    # print(res)
    # while True:
    #     time.sleep(1)
    #     print('1')
