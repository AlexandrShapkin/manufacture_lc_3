import requests

from requests.auth import HTTPBasicAuth
# todo convert top base64
auth = HTTPBasicAuth('ambiotuser_Ambiot', "dlp9^hfGHq52T")
headers = {'Content-Type': 'application/json'}
# headers = {'Content-Type': 'application/json', 'Authorization': 'Basic YW1iaW90LWRldjo6RV0yKmgnczZKcSpCcSg='}
host = 'http://10.233.14.67:10001/apiroot-nidd/v1'
scsAsId = 'Ambiot'

# todo investigate
gw_manufacture = "???"
gw_prod = "http://172.18.9.11:8081/api/v1/nbiotEvent"


# sim = 696


def create_config(external_id, manufacture=True):


    data = {
        "externalId": external_id,
        "reliableDataService": False,
        "pdnEstablishmentOption": "INDICATE_ERROR",
        "notificationDestination": gw_manufacture if manufacture else gw_manufacture
    }
    res = requests.post(host + f'/3gpp-nidd/v1/{scsAsId}/configurations', json=data, headers=headers)

    if res.status_code == 409:
        # list and delete?
        raise Exception('Такая конфигурация для этой сим-карты уже существует, обратитесь к специалисту Ambiot')
    if res.ok:
        return res.json()['self'].split('/')[-1]
    else:
        return None


def delete_config(config_id):
    res = requests.delete(host + f'/3gpp-nidd/v1/{scsAsId}/configurations/{config_id}', headers=headers)
    if not res.ok:
        x = 1
    return res.ok or res.status_code == 404
