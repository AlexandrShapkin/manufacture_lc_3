from flask import Flask, request
from threading import Thread
from waitress import serve
from multiprocessing import Process

app = Flask(__name__)


@app.route('/nb_uplink', methods=['POST'])
def uplink():
    try:
        data = request.json['externalId']
        UplinkServer.uplink_dev_eui.append(data)
    except:
        print(f'got wrong uplink - {request.data}')
        pass

    return ''


class UplinkServer:
    uplink_dev_eui = []

    def __init__(self, port=8080):
        self.server = Thread(target=serve, args=(app,), kwargs={'host': '0.0.0.0', 'port': port}, daemon=True)
        self.start()

    def start(self):
        self.server.start()

    def read_uplink_external_ids(self):
        try:
            return UplinkServer.uplink_dev_eui
        finally:
            UplinkServer.uplink_dev_eui = []
