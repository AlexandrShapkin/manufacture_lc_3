import json

import requests

# host = 'http://172.21.22.5'
# headers = {'Authorization': 'l6xtFTUBnhFWvWSU'}

# host = 'https://cloud-t.ambiot.io:444'
# headers = {'Authorization': 'DnfOuGyuLuYLiYsxcGc9dfqTpStUyWsGGd+1HzG5ZiU'}


host = 'https://cloud.ambiot.io'
headers = {'Authorization': 'RFSdQGnbwI0ghhqQaZsskr4k5o4tbk4IYuF/Xv011ls', 'Content-Type':'application/json', 'Accept':"*/*"}

# https://cloud.ambiot.io/service/devices/devEUI/D2CD5BD6E4EC6132

def get_sim_info(iccid):
    params = {"identifier": iccid}
    res = requests.get(host + '/service/nbiot/simcards', headers=headers, params=params)
    # only id??
    if res.ok:
        sim_info = res.json().get("data")
        ext_id = sim_info.get("externalId")
        sim_id = sim_info.get("id")
        # nidd_account = sim_info.get("niddAccount")
        availabiliy = sim_info.get("canUseForManufacture")
        return sim_id, ext_id, availabiliy
    else:
        raise Exception("Не выполнить запрос получения информации о сим-карте")


def create_conf(sim_id, conf_type="MANUFACTURE"):
    j_data = {
        "simCardId": sim_id,
        "configurationType": conf_type
    }
    res = requests.post(host + '/service/nbiot/configuration', headers=headers, json=j_data)
    if res.ok:
        return res.json().get('answer')
    else:
        raise Exception("Не выполнить запрос создания конфигурации сим-карты")


def check_dev_eui(device_id):
    res = requests.get(host + f'/service/devices/devEUI/{device_id}', headers=headers)
    return res.ok and res.json().get('data') is None


def create_device(sim_id, dev_eui, imei,  dev_eui_source="manufacture", device_class=19):
    j_data = {
        "devEUI": dev_eui,
        "devEUISource": dev_eui_source,
        "simCardId": sim_id,
        "deviceClass": device_class,
        "imei": imei
    }

    res = requests.post(host + '/service/devices/nbiot/create', json=j_data, headers=headers)

    if not res.ok:
        raise Exception(f"Не удалоь создать устройство в ситеме - {res.status_code}")

    j = res.json()

    return j.get('answer'), j.get('error')

# test
if __name__ == '__main__':
    iccid = "89701010051400189336"
    # print(get_sim_info(iccid))
    print(create_conf(8))
    # print(create_device(8, "0000000000000000", "qwerty"))

    x = 1