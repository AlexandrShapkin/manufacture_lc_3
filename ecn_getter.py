import manufacture_utils
from programmer import Programmer
import time

CHIP_NAME = 'ATSAMG55J19'
DEVICE_CLASS = '19'


if __name__ == '__main__':
    programmer = Programmer()

    print("<-- reading chip serial")
    programmer.connect(CHIP_NAME)
    time.sleep(1)

    print("<-- set boot bit")
    programmer.set_boot_bit()

    chip_serial = programmer.get_chip_serial_ATSAMG55J19()
    print("chip serial - " + chip_serial)

    device_id = manufacture_utils.get_8_byte_serial(chip_serial)

    device_ahid = f'{DEVICE_CLASS}-{device_id}'
    device_serial = f'ACN:{device_ahid}'

    print('-----------------------')
    print(device_serial)
