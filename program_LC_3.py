# -*- coding: utf-8 -*-
import atexit
import logging
import os
import sys
import time
from enum import IntEnum, auto

import yaml
from halo import Halo

import manufacture_utils
from api.AMLoraServiceCore import AMLoraServiceCore
from api.back import check_dev_eui, get_sim_info, create_conf, create_device
from label_printer import LabelPrinter
from manufacture_database import Databse
from programmer import Programmer
from src.sctipt_runner import ScriptRunner

from datetime import datetime

DEV = False

DB_TEST_MODE = False
FLASH_NB = True
FLASH_GPS = True
MAKE_TESTS = True
BACK_OPERATIONS = True
ONLY_PROGRAM = False
SET_APN = True

try:
    import thread
except ImportError:
    import _thread as thread


class NoParsingFilter(logging.Filter):
    def filter(self, record):
        return not record.getModule() == 'jlink'


logging.basicConfig(filename='program.log', level=logging.INFO, format='%(levelname)s: (%(funcName)s)  - %(message)s', )


# logging.addFilter(NoParsingFilter())

# ----------------------------

# nb_target_version = 'BC66NBR01A11_01.003.01.003'
nb_target_version = 'BC66NBR01A11'

# ----------------------------
CHIP_NAME = 'ATSAMG55J19'
APN = "ambiot.nidd"
# prod
# APN = "ambiot.nidd"

# \ Запуск 1 скрипта обновленияWARNING:pylink.jlink:Identified core does not match configuration. (Found: Cortex-M4, Configured: Cortex-M0)

DEVICE_CLASS = '20'
BOOT_LOADER_FILE_NAME = 'BL-NBIOT-ZHAGA-8-05.07.2021.bin'
MAIN_FIRMWARE_FILE_NAME = 'FW-NBIOT-ZHAGA-0.55-12.07.2021'

BOOT_UPDATER_LOADER_FILE_NAME = 'updater/Service_BL-NBIOT_ZHAGA.bin'
MAIN_UPDATER_FIRMWARE_FILE_NAME = 'updater/Service_FW-NBIOT_ZHAGA.bin'

MAIN_FIRMWATE_SHIFT = 0x0008E000
BOOT_LOADER_SHIFT = 0x00080000

IF_EMPTY_SERIALS = ['880c0020cd0e8000d10e8000d10e8000',
                    '00800220310140004501400045014000',
                    '00000000000000000000201000000000',
                    '00000000000000000000000000000000']

TEST_TIMEOUT = 30 + 600

# -------
KEY_DEVICE_START = 'BL VERSION'

COMMAND_TO_CHECK_AT = 'AT'

COMMAND_SIM_ICCID_KEY = '+QCCID: '
COMMAND_GET_MEASUREMENTS = 'AT+ME?'
COMMAND_GET_DIN = 'AT+DIN?'
COMMAND_GET_ALARMS = 'AT+ALRM?'
COMMAND_DIM_PATTERN = 'AT+DIMM={dim_value}'
COMMAND_SET_APN = 'AT+APN={apn}'

COMMAND_GET_SIM_ICCID = 'AT+NBIOTCMD=AT+QCCID'
COMMAND_GET_IMEI = 'AT+NBIOTCMD=AT+CGSN=1'
COMMAND_SEND_UPLINK = 'AT+MACTST'
COMMAND_CHECK_NB_VERSTION = 'AT+NBIOTCMD=ATI'
COMMAND_CHECK_NETWORK_REGISTRATION = 'AT+NBIOTCMD=AT+CEREG?'
KEY_OK_NETWORK_REGISTRATION = '+CEREG: 0,1'

KEY_OK = 'OK'

KEY_ERROR = 'ERROR'

# ------
ALRM_FLASH_ERR_INDEX = 17
ALRM_ACCELEROMETER_ERR_INDEX = 13
ALRM_I2C_ERR_INDEX = 14
ALRM_DIM_CHANNEL_ERR_INDEX = 23
ALRM_GENERATOR_ERR_INDEX = 24
ALRM_MEASUREMENTS_INDEX = 15
ALRM_GPS_ERR_INDEX = 12


# ------

class State(IntEnum):
    PREPARING = auto()
    FLASHING_BOOT_UPDATER = auto()
    FLASHING_MAIN_UPDATER = auto()
    FLASHING_NB_CHIP = auto()
    # todo
    # FLASHING_GPS_CHIP = auto()
    FLASHING_BOOT = auto()
    FLASHING_MAIN = auto()
    DEVICE_LOADING = auto()
    CHECK_AT = auto()
    CHECK_NB_VERSION = auto()
    # todo
    # CHECK_GPS_VERSION = auto()
    CHECK_ALARMS = auto()
    CHECK_MEASUREMENTS = auto()
    CHECK_DIM_0 = auto()
    CHECK_DIM_20 = auto()
    CHECK_DIM_50 = auto()
    CHECK_DIN_FALSE = auto()
    CHECK_DIN_TRUE = auto()
    GET_ICCID = auto()
    GET_IMEI = auto()
    CHECK_CONNETION = auto()
    CHECK_UPLINK = auto()

    SUCCESS = auto()
    ERROR = auto()


# -------
class Res:
    timeout_thread = None
    state = None
    d_in_1_test = None
    fw = ''
    fs = ''
    error = None

    plc_connect = None
    server_connect = None

    voltage = None
    current = None
    fr = None
    power = None

    @staticmethod
    def get_json():
        return {
            'status': Res.state,
            'error': Res.error,
            'plc_connect': Res.plc_connect,
            'voltage': Res.voltage,
            'fr': Res.fr,
            'fs': Res.fs,
            'power': Res.power,
            'current': Res.current,
            'fw': Res.fw,
            'd_in_1_test': Res.d_in_1_test,
        }

    @staticmethod
    def check():
        return (
                       Res.d_in_1_test is not None and
                       Res.voltage is not None and
                       Res.server_connect is not None and
                       Res.plc_connect is not None and
                       Res.fr is not None and
                       # Res.fs_mounted is not None and
                       Res.power is not None
               ) or Res.error is not None


def confirm(str):
    while True:
        r = input(str + " - press y or n and Enter: ")
        if r == "y":
            return True
            break
        elif r == "n":
            return False
            break
        else:
            print("Please enter only  y or n")


def check_power(pow):
    # todo
    return pow >= 0 and pow < 200


def test_timeout():
    if Res.timeout_thread is not None:
        pass
        # Res.timeout_thread.s
    print("<-- init timeout")
    time.sleep(TEST_TIMEOUT)
    if Res.check() or Res.error is not None:
        Res.error = "Timeout!"


def command_timeout(state, long=20):
    print("<-- init comand timeout")
    time.sleep(long)
    if Res.state == state:
        Res.error = f"Timeout command {state.name}"


def check_raise_error(out='', m=''):
    if KEY_ERROR in out or Res.error is not None:
        raise Exception(Res.error + " - " + m)


def parse_measurements(data):
    # u0 u1 i0 i1 c0 c1 f0 f1 p0 p1 OK
    return None


def get_fw_file_path(application_path, file_name):
    return application_path + '/firmware_files/' + file_name


def s16(value):
    return -(value & 0x8000) | (value & 0x7fff)


def main():
    global sr, sp, mq

    t_main = datetime.now()


    print("======== START PROGRAMMING ======== ")
    print('КЛАСС УСТРОЙСТВА - ' + DEVICE_CLASS)

    if getattr(sys, "frozen", False):
        application_path = os.path.dirname(sys.executable)
    elif __file__:
        application_path = os.path.dirname(__file__)

    t_get_settings = datetime.now()

    sp = Halo()
    sp.start("Подключение к БД")

    db = Databse()
    sp.succeed('Соединение установлено')

    sp.start("Чтение настроек")
    with open("config.yaml", 'r') as stream:
        config_yaml = yaml.safe_load(stream)

    if config_yaml.get('com_port_loop') is None or config_yaml.get('com_port_loop').get(
            'com_in') is None or config_yaml.get('com_port_loop').get('com_out') is None:
        raise Exception('Проверьте файл конфигурации (config.yaml)')
    sp.succeed('Настройки получены')



    try:
        action_message = "Инициализация устройств"
        sp.start(action_message)
        programmer = Programmer()
        sp.succeed('Программатор подключен')

        if not DEV:
            sp.start(action_message)
            printer = LabelPrinter()
            sp.succeed('Принтер подключен')

        action_message = "Чтение серийника чипа"
        sp.start(action_message)
        programmer.connect(CHIP_NAME)
        time.sleep(1)
        programmer.set_boot_bit()

        chip_serial = programmer.get_chip_serial_ATSAMG55J19()
        sp.succeed("Серийник чипа получен - " + chip_serial)
        logging.info("Серийник чипа получен - " + chip_serial)

        if chip_serial in IF_EMPTY_SERIALS:
            e_mess = 'Получен некорректный серийник'
            sp.fail(e_mess)
            raise Exception(e_mess)

        sp.start('Получение информациио серинике')
        chip_record = db.get_chip_record(chip_serial)
        device_id = None
        chip_record_id = None
        nbiot_updated = False
        gps_updated = False

        if chip_record is None:
            device_id = manufacture_utils.get_8_byte_serial(chip_serial)
            chip_record_id = db.add_chip(chip_serial, device_id)
            if BACK_OPERATIONS:
                if not check_dev_eui(device_id):
                    raise Exception("Device already on back")

            sp.succeed('Серийник добавлен')
        else:
            sp.succeed('Данные получены')
            device_id = chip_record[2]
            chip_record_id = chip_record[0]
            # sim_card_id = chip_record[4]
            nbiot_updated = chip_record[5]
            gps_updated = chip_record[6]

            device_serial = f'ACN:{DEVICE_CLASS}-{device_id}'

            # if db.check_if_serial_hass_success_attempt(chip_serial):
            #     if not confirm(
            #             f"Устройство с таким адресом - {device_serial} уже было успешно произведено! Вы уверены что хотите пролжить?"):
            #         sys.exit()

        attempt_id = db.start_attempt(chip_record_id)

        if attempt_id is None:
            raise Exception("DB ERROR - creating attempt, press ENTER to exit...")

        db.update_attempt_status(attempt_id, State.PREPARING.name)
        device_serial = f'ACN:{DEVICE_CLASS}-{device_id}'
        sp.info(f'Адрес будет: {device_serial}')

        print(f't_get_settings - {(datetime.now()-t_get_settings).seconds}')

        gps_update_now = True
            # FLASH_GPS and (not gps_updated or (gps_updated and confirm(
            # 'GPS модуль этого контроллера уже был прошит, хотите прошить его повторно?')))
        nbiot_update_now = True
            # FLASH_NB and (not nbiot_updated or (nbiot_updated and confirm(
            # 'NBIoT модуль этого контроллера уже был прошит, хотите прошить его повторно?')))

        if nbiot_update_now or gps_update_now:
            sr = ScriptRunner(sp, config_yaml, application_path)

        # sr.update_gps()

        # fixme
        nbiot_update_now = False
        gps_update_now = False

        if nbiot_update_now:

            t_burn_service = datetime.now()

            db.update_attempt_status(attempt_id, State.FLASHING_NB_CHIP.name)
            sp.info('Прошивка модуля сервисным ПО:')
            action_name = 'Подготовка основного чипа'
            programmer = Programmer()
            programmer.connect(CHIP_NAME)
            programmer.erase()
            sp.succeed('Модуль очищен')
            sp.start(action_name)
            programmer.set_boot_bit()
            programmer.reset()
            db.update_attempt_status(attempt_id, State.FLASHING_BOOT_UPDATER.name)
            programmer.flash(get_fw_file_path(application_path, BOOT_UPDATER_LOADER_FILE_NAME), BOOT_LOADER_SHIFT)
            sp.succeed('Бутлоадер загружен')
            sp.start(action_name)
            db.update_attempt_status(attempt_id, State.FLASHING_MAIN_UPDATER.name)
            programmer.flash(get_fw_file_path(application_path, MAIN_UPDATER_FIRMWARE_FILE_NAME), MAIN_FIRMWATE_SHIFT)
            sp.succeed('Основная прошивка загружена')
            sp.start(action_name)
            programmer.reset()
            programmer.restart()

            print(f't_burn_service - {(datetime.now()-t_burn_service).seconds}')

            sp.start('Ожидание запуска')
            time.sleep(10)
            sp.succeed('Модуль готов к обнолвению NbIoT')
            programmer.jlink.close()

            t_nbiot = datetime.now()
            sr.update_nbiot()
            print(f't_nbiot - {(datetime.now()-t_nbiot).seconds}')
            db.set_nbiot_updated(chip_record_id)
            clean_procs()
            time.sleep(2)
            sp.info("Вспомогательные скрипты закрыты")

        if gps_update_now:
            t_burn_service = datetime.now()

            sp.info('Прошивка модуля сервисным ПО:')
            action_name = 'Подготовка основного чипа'
            programmer = Programmer()
            programmer.connect(CHIP_NAME)
            programmer.erase()
            sp.succeed('Модуль очищен')
            sp.start(action_name)
            programmer.set_boot_bit()
            programmer.reset()
            db.update_attempt_status(attempt_id, State.FLASHING_BOOT_UPDATER.name)
            programmer.flash(get_fw_file_path(application_path, BOOT_UPDATER_LOADER_FILE_NAME), BOOT_LOADER_SHIFT)
            sp.succeed('Бутлоадер загружен')
            sp.start(action_name)
            db.update_attempt_status(attempt_id, State.FLASHING_MAIN_UPDATER.name)
            programmer.flash(get_fw_file_path(application_path, MAIN_UPDATER_FIRMWARE_FILE_NAME), MAIN_FIRMWATE_SHIFT)
            sp.succeed('Основная прошивка загружена')
            sp.start(action_name)
            programmer.reset()
            programmer.restart()

            print(f't_burn_service - {(datetime.now()-t_burn_service).seconds}')

            sp.start('Ожидание запуска')
            time.sleep(10)
            sp.succeed('Модуль готов к обнолвению переферийных контроллеров')
            programmer.jlink.close()
            t_gps = datetime.now()
            sr.update_gps()
            print(f't_gps - {(datetime.now()-t_gps).seconds}')
            db.set_gps_updated(chip_record_id)
            clean_procs()
            time.sleep(2)
            sp.info("Вспомогательные скрипты закрыты")

        # ---------------------------------------------------------------
        t_burn_main = datetime.now()
        action_message = 'Прошивка основного модуля'
        sp.start(action_message)
        programmer = Programmer()
        programmer.connect(CHIP_NAME)
        programmer.erase()
        sp.info('Модуль очищен')
        sp.start(action_message)
        programmer.set_boot_bit()
        programmer.reset()
        db.update_attempt_status(attempt_id, State.FLASHING_BOOT.name)
        programmer.flash(get_fw_file_path(application_path, BOOT_LOADER_FILE_NAME), BOOT_LOADER_SHIFT)
        sp.info('Бутлоадер загружен')
        sp.start(action_message)
        db.update_attempt_status(attempt_id, State.FLASHING_MAIN.name)
        Res.fw = MAIN_FIRMWARE_FILE_NAME
        programmer.flash(get_fw_file_path(application_path, MAIN_FIRMWARE_FILE_NAME), MAIN_FIRMWATE_SHIFT)
        sp.info('Основная прошивка загружена')
        sp.start(action_message)
        programmer.reset()
        programmer.restart()
        time.sleep(2)
        sp.succeed('Модуль прошит')

        print(f't_burn_main - {(datetime.now()-t_burn_main).seconds}')

        if ONLY_PROGRAM:
            programmer.jlink.close()
            sp.succeed('successfully flashed and out')
            return

        sp.start('Ожидание запуска модуля')
        time.sleep(3)
        programmer = Programmer()
        programmer.connect(CHIP_NAME)
        time.sleep(3)
        # programmer.restart()
        programmer.jlink.rtt_start()
        time.sleep(5)
        out = programmer.read_rtt(180)
        # print("RTT - " + str(out))
        if KEY_DEVICE_START in out:
            sp.info('Модуль запущен')
            db.update_attempt_status(attempt_id, State.CHECK_AT.name)
        else:
            Res.error = "Error on device start"
            check_raise_error(out)

        sp.info('Тестирование модуля')
        sp.info('--------------------------------------------')

        t_auto_test = datetime.now()

        sp.start('Проверка AT интерфейса')
        programmer.writeToRTT(COMMAND_TO_CHECK_AT)
        out = programmer.read_rtt()
        if KEY_OK in out:
            sp.succeed("AT интерфейс")
        else:
            sp.fail('Error on Check AT')
            raise Exception('Error on Check AT')

        if SET_APN:
            sp.start('Установка APN')
            programmer.writeToRTT(COMMAND_SET_APN.format(apn=APN))
            out = programmer.read_rtt()
            if KEY_OK in out:
                sp.succeed("APN установлен")
            else:
                sp.fail('Не удалось задать APN')
                raise Exception('Не удалось задать APN')



        # programmer.writeToRTT("AT+NBIOTCMD=AT+CFUN=1")
        # out = programmer.read_rtt()
        #
        # if KEY_OK not in out:
        #     raise Exception("Не удалось связаться с NbIoT")

        time.sleep(5)
        sp.start('Проверка версии NBIoT модуля')
        programmer.writeToRTT(COMMAND_CHECK_NB_VERSTION)
        i = 0
        tmp_nb_upd = False
        while i < 2:
            i += 1
            time.sleep(2)
            out = programmer.read_rtt(timeout=30)
            print(out)
            if "Revision" in out:
                if nb_target_version in out:
                    tmp_nb_upd = True
                break
        #
        if not tmp_nb_upd:
            db.set_nbiot_updated(chip_record_id, False)
            raise Exception("NBIoT модуль не обновлен")

        db.set_nbiot_updated(chip_record_id)
        sp.start('NBIoT модуль обновлен')

        if MAKE_TESTS:
            sp.start('Внутренняя проверка несиправностей')
            time.sleep(5)
            programmer.writeToRTT(COMMAND_GET_ALARMS)
            db.update_attempt_status(attempt_id, State.CHECK_ALARMS.name)
            out = programmer.read_rtt()
            if KEY_OK in out:
                sp.info("Данные внутренней проверки получены")
            else:
                raise Exception("Error on get Alarms")

            alrs = out.strip().replace("\r\n", ' ').split(' ')[1]
            if alrs != '0':
                t_err = 'ALARMS - '
                bi = format(int(alrs, 16), '032b')
                if bi[-1 - ALRM_FLASH_ERR_INDEX] == '1':
                    t_err += 'flash, '
                if bi[-1 - ALRM_ACCELEROMETER_ERR_INDEX] == '1':
                    t_err += 'accelerometer, '
                if bi[-1 - ALRM_I2C_ERR_INDEX] == '1':
                    t_err += 'i2c, '
                if bi[-1 - ALRM_DIM_CHANNEL_ERR_INDEX] == '1':
                    t_err += 'dim channel, '
                if bi[-1 - ALRM_GENERATOR_ERR_INDEX] == '1':
                    t_err += 'generator, '
                if bi[-1 - ALRM_MEASUREMENTS_INDEX] == '1':
                    t_err += 'measuremets, '
                if bi[-1 - ALRM_GPS_ERR_INDEX] == '1':
                    t_err += 'gps not updated, '
                    db.set_gps_updated(chip_record_id, False)

                Res.error = t_err
                check_raise_error(t_err)
            sp.succeed("Внутренних неисправностей не обнаружено")

            print(f't_auto_test - {(datetime.now()-t_auto_test).seconds}')
            t_man_tests = datetime.now()

            sp.start('Получение показаний измерителя')
            db.update_attempt_status(attempt_id, State.CHECK_MEASUREMENTS.name)
            programmer.writeToRTT(COMMAND_GET_MEASUREMENTS)
            out = programmer.read_rtt()

            if KEY_OK in out:
                sp.info("Данные измерителя получены")
            else:
                raise Exception("Error getting measurements")

            tmp = out.strip().split("\r\n")
            ms = tmp[0].split(" ")[1]
            payload_bin = bytes.fromhex(ms)
            u = int.from_bytes(payload_bin[0:2], byteorder='big', signed=False) / 100
            i = int.from_bytes(payload_bin[2:4], byteorder='big', signed=False) / 100
            c = int.from_bytes(payload_bin[4:6], byteorder='big', signed=True) / 100
            f = int.from_bytes(payload_bin[6:8], byteorder='big', signed=False) / 100
            p = int.from_bytes(payload_bin[8:10], byteorder='big', signed=False) / 100
            Res.voltage = u
            Res.current = i
            Res.fr = f
            Res.power = p
            if not check_power(p):
                Res.error = 'Power check failed!'
                raise Exception('Power check failed!')
            sp.info(f'Показания измерителя - u={u}, i={i}, c={c}, f={f}, p={p}')
            if not confirm("подтвердите показания"):
                e_mess = 'Показания измерителя не подтверждены'
                sp.fail(e_mess)
                raise Exception('Показания измерителя не подтверждены')
            # DIM
            input("Press ENTER to start DIM")
            Res.state = State.CHECK_DIM_0
            programmer.writeToRTT(COMMAND_DIM_PATTERN.format(dim_value=0))
            db.update_attempt_status(attempt_id, Res.state.name)
            out = programmer.read_rtt()
            if KEY_OK in out:
                sp.info("Команда диммирования применена")
            else:
                Res.error = "Error on DIM 0"
                check_raise_error(out)
            if not confirm("Подтвердите диммирование 0%"):
                Res.error = "DIM on 0 not confirmed"
                raise Exception("DIM on 0 not confirmed")
            Res.state = State.CHECK_DIM_20
            programmer.writeToRTT(COMMAND_DIM_PATTERN.format(dim_value=50))
            db.update_attempt_status(attempt_id, Res.state.name)
            out = programmer.read_rtt()
            if KEY_OK in out:
                print("Dimmed")
            else:
                Res.error = "Error on DIM 50"
                check_raise_error(out)
            if not confirm("Подтвердите диммирование 50%"):
                Res.error = "DIM on 50 not confirmed"
                raise Exception("DIM on 50 not confirmed")
            Res.state = State.CHECK_DIM_50
            programmer.writeToRTT(COMMAND_DIM_PATTERN.format(dim_value=100))
            db.update_attempt_status(attempt_id, Res.state.name)
            out = programmer.read_rtt()
            if KEY_OK in out:
                print("Dimmed")
            else:
                Res.error = "Error on DIM 100"
                check_raise_error(out)
            if not confirm("Подтвердите диммирование 100%"):
                Res.error = "DIM on 100 not confirmed"
                raise Exception("DIM on 100 not confirmed")
            # fixme when new FW will be available
            # Res.state = State.CHECK_DIN_FALSE
            # programmer.writeToRTT(COMMAND_GET_DIN)
            # db.update_attempt_status(attempt_id, Res.state.name)
            # out = programmer.read_rtt()
            #
            # if KEY_OK in out:
            #     print("Got DINs 0")
            # else:
            #     Res.error = "Error get DIN statuses"
            #     check_raise_error(out)
            #
            # tmp = out.strip().replace("\r\n", ' ').split(' ')[1]
            # if int(tmp, 16) != 0:
            #     Res.error = f"Failed DIN check - must be 0: {tmp}"
            #     raise Exception(f"Failed DIN check - must be 0: {tmp}")
            #
            # Res.state = State.CHECK_DIN_TRUE
            # input("Connect all Input channels and press ENTER....")
            # programmer.writeToRTT(COMMAND_GET_DIN)
            # db.update_attempt_status(attempt_id, Res.state.name)
            # out = programmer.read_rtt()
            #
            # if KEY_OK in out:
            #     print("Got DINs 1")
            # else:
            #     Res.error = "Error get DIN statuses"
            #     check_raise_error(out, Res.error)
            #
            # tmp = out.strip().replace("\r\n", ' ').split(' ')[1]
            # if int(16) != 1:
            #     Res.error = f"Failed DIN check - must be 1: {tmp}"
            #     raise Exception(f"Failed DIN check - must be 1: {tmp}")
            # Res.d_in_1_test = True

        print(f't_man_tests - {(datetime.now()-t_man_tests).seconds}')
        t_uplink = datetime.now()
        sp.start('Получение ICCID сим-карты')
        programmer.writeToRTT(COMMAND_GET_SIM_ICCID)
        db.update_attempt_status(attempt_id, State.GET_ICCID)
        time.sleep(3)
        out = programmer.read_rtt()
        # print(out)
        if KEY_OK in out:
            pass
        else:
            Res.error = "Error get ICCID"
            check_raise_error(out)

        if 'QCCID' not in out:
            Res.error = "Error get ICCID"
            check_raise_error("Error get ICCID" + out)

        iccid = out[out.find('+QCCID:') + 8:].split('\r\n')[0]

        if not iccid.isnumeric():
            raise Exception('Не удалось корректно считать ICCID')

        logging.info(f"iccid - {iccid}")

        sp.start("Считывание IMEI")
        programmer.writeToRTT(COMMAND_GET_IMEI)
        db.update_attempt_status(attempt_id, State.GET_IMEI)
        time.sleep(3)
        out = programmer.read_rtt()
        # print(out)
        if KEY_OK in out:
            pass
        else:
            Res.error = "Error get IMEI"
            check_raise_error(out)

        if 'CGSN' not in out:
            Res.error = "Error get IMEI"
            check_raise_error("Error get ICCID" + out)

        imei = out[out.find('+CGSN:') + 7:].split('\r\n')[0]

        if not iccid.isnumeric():
            raise Exception('Не удалось корректно считать ICCID')

        sp.succeed(f'IMEI считан - {imei}')


        sp.start('Запрос данных о сим-карте')
        sim_id, sim_ext_id, availabiliy = get_sim_info(iccid)
        if sim_id is None or sim_ext_id == None:
            sp.fail('Нет данных по этой сим-карте')
            raise Exception(f"No sim with this iccid - {iccid}")

        if not availabiliy:
            raise Exception(
                f"Данная сим-карта{iccid} не может быть ипользована для производства, обратитесь к предстваителю Ambiot")

        mq = AMLoraServiceCore()

        sp.start("connecting MQ")
        while not mq._connected:
            time.sleep(1)
        #
        sp.succeed("connected MQ")

        sp.start('Добавление конфигурации сим-карты')
        if not create_conf(sim_id):
            raise Exception('Не удлось создать конфигурацию sim карты')
        sp.succeed('Конфигурация сим-карты создана')

        pause = 5
        max_attempts = round((60 * 2) / pause)
        sp.start('Подключеие модуля к сети')
        i = 0
        while True:
            if i > max_attempts:
                e_mess = 'Устройству не удалось подключиться к сети'
                sp.fail(e_mess)
                raise Exception(e_mess)
            i += 1
            programmer.writeToRTT(COMMAND_CHECK_NETWORK_REGISTRATION)
            time.sleep(3)
            out = programmer.read_rtt(raiseTimeoutEx=False)
            # print(out)
            if KEY_OK not in out:
                e_mess = 'Не удалось провверить подключение модуля к сети'
                sp.fail(e_mess)
                raise Exception(e_mess)
            if KEY_OK_NETWORK_REGISTRATION in out:
                sp.succeed('Устройство подключилось к сети')
                break
            time.sleep(pause)
        sp.succeed('Модуль подключился к сети')

        mq.set_wait_uplink_ext_id(sim_ext_id)
        pause = 2
        max_attempts = round((60 * 1) / pause)
        sp.start('Отправка аплинка')
        programmer.writeToRTT(COMMAND_SEND_UPLINK)
        out = programmer.read_rtt()
        if KEY_OK not in out:
            raise Exception('Не удалось отправить ааплинк')
        sp.info('Аплинк отправлен')

        sp.start('Ожидние подтверждения апплинка')
        mq.set_uplink_waiter()

        if BACK_OPERATIONS:
            res, err = create_device(sim_id, device_id, imei, device_class=DEVICE_CLASS)
            if not res:
                raise Exception(f'не удалось создать устройство в сисстеме ambiot - {err}')


        print(f't_uplink - {(datetime.now()-t_uplink).seconds}')
        print(f't_main - {(datetime.now()-t_main).seconds}')


        db.add_test_results(attempt_id, str(Res.get_json()))


    except programmer.TimeoutException:
        sp.stop()
        Res.error = "Timeout reading from RTT!"
    except Exception as e:
        sp.stop()
        Res.error = f"Exception - {e}"
        print(e)
        raise
    finally:
        logging.info(f"sending results {Res.get_json()}")
        try:
            db.add_test_results(attempt_id, str(Res.get_json()))
        except:
            pass
        sp.stop()
        clean_procs()
        try:
            mq.disconnct()
        except:
            pass
        if programmer.jlink.opened():
            programmer.jlink.rtt_stop()

        #     todo set sim produced

    print("====================================")
    if Res.error is not None:
        db.update_attempt_status(attempt_id, State.ERROR.name)
        print('ERROR - ' + Res.error)
    else:

        db.update_attempt_status(attempt_id, State.SUCCESS.name)
        print("SUCCESS")
        print(device_id)

        if not DEV:
            printer.print(device_id, DEVICE_CLASS)
            # fixme  withoout pring first time
            # print("Lock")
            # programmer.set_security_bit()

    programmer.jlink.close()
    print("====================================")


def clean_procs():
    global sr
    try:
        if sr is not None:
            sr.clean_procs()
    except:
        pass


if __name__ == '__main__':
    atexit.register(clean_procs)

    # ------------
    import sys

    try:
        sys.getwindowsversion()
    except AttributeError:
        isWindows = False
    else:
        isWindows = True

    if isWindows:
        # Based on:
        #   "Recipe 496767: Set Process Priority In Windows" on ActiveState
        #   http://code.activestate.com/recipes/496767/
        import win32api, win32process, win32con

        pid = win32api.GetCurrentProcessId()
        handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
        win32process.SetPriorityClass(handle, win32process.HIGH_PRIORITY_CLASS)
    else:
        import os

        os.nice(1)
    # ------------

    while True:
        try:
            main()
        except Exception as e:
            print(f'ERROR: {e}')
            raise e
            # pass
        input("Press ENTER to exit...")
        break
